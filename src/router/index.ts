import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import AdminPanel from '../pages/AdminPanel.vue'
import Login from '../pages/Login.vue'
import Profile from '../pages/Profile.vue'
import UserService from '../services/UserService'

const routes: Array<RouteRecordRaw> = [
  {
    name: 'AdminPanel',
    path: '/',
    component: AdminPanel,
    meta: {
      requiresAuth: true,
    },
  },
  {
    name: 'Login',
    path: '/login',
    component: Login,
  },
  {
    name: 'Profile',
    path: '/profile',
    component: Profile,
    meta: {
      requiresAuth: true,
    },
  },
]

const router = createRouter({
  history: createWebHistory(),
  routes,
})

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (!UserService.isLoggedIn()) {
      next({ name: 'Login' })
    } else {
      next()
    }
  } else {
    if (UserService.isLoggedIn() && to.path === '/login') {
      next({ name: 'Profile' })
    } else {
      next()
    }
  }
})

export default router
