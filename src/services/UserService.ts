import User from '@/models/User'

export default class UserService {
  static setSession(user: User): void {
    localStorage.setItem('current_user', JSON.stringify(user))
  }

  static endSession(): void {
    localStorage.removeItem('current_user')
  }

  static isLoggedIn(): boolean {
    return localStorage.getItem('current_user') != null
  }
}
