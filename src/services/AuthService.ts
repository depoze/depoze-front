import axios, { AxiosResponse } from 'axios'
import UserService from './UserService'

export default class AuthService {
  static login(
    email: string,
    password: string
  ): Promise<AxiosResponse<any, any>> {
    return axios.post(`${import.meta.env.VITE_SERVER_URL}/login`, {
      username: email,
      password,
    })
  }

  static signOut(): void {
    UserService.endSession()
  }
}
